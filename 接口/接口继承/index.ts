interface People {
  name: string
}

interface China extends People {
  language: string
}

interface Usa extends People {
  skinColor: string
}

interface HalfBlood extends China, Usa {
  // 也可以继承多个
  weight: number
}

let Jessie: HalfBlood = {
  name: 'Jessie',
  language: 'Chinese',
  skinColor: 'yellow',
  weight: 50,
}
