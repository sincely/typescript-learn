// TypeScript 里，undefined 和 null 两者有各自的类型分别为 undefined 和 null。

let u: undefined = undefined
let n: null = null

// 默认情况下null和undefined是所有类型的子类型。 就是说你可以把 null和undefined赋值给number类型的变量。
// 然而，如果你指定了--strictNullChecks 标记，null 和 undefined 只能赋值给 void 和它们各自的类型。
