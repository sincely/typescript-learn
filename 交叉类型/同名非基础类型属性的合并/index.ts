interface D { d: boolean; }
interface E { e: string; }
interface F { f: number; }

interface A { x: D; }
interface B { x: E; }
interface C { x: F; }

type ABC = A & B & C;

let abc: ABC = {
  x: {
    d: true,
    e: 'semlinker',
    f: 666
  }
};
// 在混入多个类型时，若存在相同的成员，且成员类型为非基本数据类型，那么是可以成功合并。
console.log('abc:', abc);
