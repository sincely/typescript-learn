// 类型别名用来给一个类型起个新名字。

type Message = string | string[]

let greet = (message: Message) => {
  // ...
}
