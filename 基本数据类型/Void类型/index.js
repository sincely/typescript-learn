// 某种程度上来说，void 类型像是与 any 类型相反，它表示没有任何类型。
// 当一个函数没有返回值时，你通常会见到其返回值类型是 void：
// 声明函数返回值为void
function warnUser() {
    console.log('This is my warning message');
}
// 需要注意的是，声明一个 void 类型的变量没有什么作用，因为在严格模式下，它的值只能为 undefined
var unusable = undefined;
console.log(unusable);
