// 就像所有类型都可以赋值给 any，所有类型也都可以赋值给 unknown。
// 这使得 unknown 成为 TypeScript 类型系统的另一种顶级类型（另一种是 any）。
// 下面我们来看一下 unknown 类型的使用示例：
// 不知道的类型
let value: unknown

value = true // OK
value = 42 // OK
value = 'Hello World' // OK
value = [] // OK
value = {} // OK
value = Math.random // OK
value = null // OK
value = undefined // OK
value = new TypeError() // OK
value = Symbol('type') // OK

// 对value变量的所有赋值都被认为是类型正确的。但是，当我们尝试将类型为unknown的值赋值给其他类型的变量时会发生什么？

// let value: unknown;

// let value1: unknown = value; // OK
// let value2: any = value; // OK
// let value3: boolean = value; // Error
// let value4: number = value; // Error
// let value5: string = value; // Error
// let value6: object = value; // Error
// let value7: any[] = value; // Error
// let value8: Function = value; // Error

// unknown 类型只能被赋值给 any 类型和 unknown 类型本身。
// 直观地说，这是有道理的：只有能够保存任意类型值的容器才能保存 unknown 类型的值。
// 毕竟我们不知道变量value中存储了什么类型的值。
// 现在让我们看看当我们尝试对类型为 unknown 的值执行操作时会发生什么。以下是我们在之前 any 章节看过的相同操作：

// let value1: unknown

// value1.foo.bar // Error
// value1.trim() // Error
// value1() // Error
// new value1() // Error
// value1[0][1] // Error

// 将value变量类型设置为unknown后，这些操作都不再被认为是类型正确的。
// 通过将any类型改变为unknown类型，我们已将允许所有更改的默认设置，更改为禁止任何更改。
