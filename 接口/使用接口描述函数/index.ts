// 我们还可以使用接口来描述一个函数
// 返回值必须一致；参数可以不写，写了必须对应上
// 函数的参数名不需要与接口里定义的名字相匹配。
// 可改变函数的参数名，只要函数参数的位置不变
interface userInfoFunc {
  // 返回类型为 string
  (age: number, name: string): string
}

let exampleFuncOne: userInfoFunc = function (age: number, name: string) {
  return `${name}今年${age}岁了`
}

let exampleFuncTwo: userInfoFunc = (year: number, monicker: string) => {
  return `${monicker}今年${year}岁了`
}

// 注意，如果使用接口来单独描述一个函数，是没 key 的，因为函数是没有属性的，只有参数和返回值类型
