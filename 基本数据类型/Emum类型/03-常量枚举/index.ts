// 除了数字枚举和字符串枚举之外，还有一种特殊的枚举 —— 常量枚举。
// 它是使用 const 关键字修饰的枚举，常量枚举会使用内联语法，
// 不会为枚举类型编译生成任何 JavaScript。为了更好地理解这句话，我们来看一个具体的例子：
const enum Direction {
  NORTH,
  SOUTH,
  EAST,
  WEST,
}

let dir: Direction = Direction.NORTH
console.log(dir) // 0)
// 以上代码对应的 ES5 代码如下：

// "use strict";
// var dir = 0 /* NORTH */;
