// 在一个字符串枚举里，每个成员都必须用字符串字面量，或另外一个字符串枚举成员进行初始化。
enum Direction {
  NORTH = 'NORTH',
  SOUTH = 'SOUTH',
  EAST = 'EAST',
  WEST = 'WEST'
}

// 以上代码对应的 ES5 代码如下：

// "use strict";
// var Direction;
// (function (Direction) {
//     Direction["NORTH"] = "NORTH";
//     Direction["SOUTH"] = "SOUTH";
//     Direction["EAST"] = "EAST";
//     Direction["WEST"] = "WEST";
// })(Direction || (Direction = {}));

enum Direction1 {
  NORTH,
  SOUTH,
  EAST,
  WEST
}

let dirName = Direction1[0] // NORTH
let dirVal = Direction1['NORTH'] // 0

// 通过观察数字枚举和字符串枚举的编译结果，我们可以知道数字枚举除了支持 从成员名称到成员值 的普通映射之外，
// 它还支持 从成员值到成员名称 的反向映射：

// 另外，对于纯字符串枚举，我们不能省略任何初始化程序。
// 而数字枚举如果没有显式设置值时，则会使用默认规则进行初始化。
