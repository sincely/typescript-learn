// 在 TypeScript 中，任何类型都可以被归为 any 类型。
// 这让 any 类型成为了类型系统的顶级类型（也被称作全局超级类型）。
var notSure = 666;
notSure = 'semlinker';
notSure = false;
// any 类型本质上是类型系统的一个逃逸舱。作为开发者，这给了我们很大的自由：
// TypeScript 允许我们对 any 类型的值执行任何操作，而无需事先执行任何形式的检查。比如：
var value;
value.foo.bar; // OK
value.trim(); // OK
value(); // OK
new value(); // OK
value[0][1]; // OK
// 在许多场景下，这太宽松了。使用 any 类型，可以很容易地编写类型正确但在运行时有问题的代码。
// 如果我们使用 any 类型，就无法使用 TypeScript 提供的大量的保护机制。
// 为了解决 any 带来的问题，TypeScript 3.0 引入了 unknown 类型。
