// 接口可以用来描述一个具体的对象、描述函数类型或者描述类的类型

// 描述一个具体的对象

interface IOption {
  label: string
  value: number
}

let obj: IOption = {
  label: 'key',
  value: 1,
}
