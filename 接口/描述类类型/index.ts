//  描述类类型，使用 implements 实现接口
interface IAnimal {
  name: string
  age: number
}

class Panda implements IAnimal {
  name = 'mike'
  age = 18
}
