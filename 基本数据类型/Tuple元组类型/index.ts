// 众所周知，数组一般由同种类型的值组成，但有时我们需要在单个变量中存储不同类型的值，
// 这时候我们就可以使用元组。在 JavaScript 中是没有元组的，元组是 TypeScript 中特有的类型，其工作方式类似于数组。

// 元组可用于定义具有有限数量的未命名属性的类型。每个属性都有一个关联的类型。
// 使用元组时，必须提供每个属性的值。为了更直观地理解元组的概念，我们来看一个具体的例子：
let tupleType: [string, boolean]
tupleType = ['semlinker', true]

// 在上面代码中，我们定义了一个名为 tupleType 的变量，它的类型是一个类型数组[string, boolean]，
// 然后我们按照正确的类型依次初始化 tupleType 变量。与数组一样，我们可以通过下标来访问元组中的元素：

console.log(tupleType[0]) // semlinker
console.log(tupleType[1]) // true

// 在元组初始化的时候，如果出现类型不匹配的话，比如：

// tupleType = [true, 'semlinker']

// 此时，TypeScript 编译器会提示以下错误信息：

// [0]: Type 'true' is not assignable to type 'string'.
// [1]: Type 'string' is not assignable to type 'boolean'.

// 很明显是因为类型不匹配导致的。在元组初始化的时候，我们还必须提供每个属性的值，不然也会出现错误，比如：

// tupleType = ['semlinker']

// 不能将类型“[string]”分配给类型“[string, boolean]”。源具有 1 个元素，但目标需要 2 个。t
