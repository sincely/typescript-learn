// 可选参数
function createUserId(name: string, id: number, age?: number): string {
  return name + id + age;
}

// 默认参数
function createUser(
  name: string = "semlinker",
  id: number,
  age?: number
): string {
  return name + id;
}


// createUserId("a", 1);
console.log(createUserId("a", 1, 15));