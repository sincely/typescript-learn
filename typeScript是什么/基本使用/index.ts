function greet(person: string) {
  return 'Hello, ' + person
}

console.log(greet('TypeScript'))

// 然后执行 tsc hello.ts 命令，之后会生成一个编译好的文件 hello.js：

// 观察以上编译后的输出结果，我们发现 person 参数的类型信息在编译后被擦除了。
// TypeScript 只会在编译阶段对类型进行静态检查，如果发现有错误，编译时就会报错。
// 而在运行时，编译生成的 JS 与普通的 JavaScript 文件一样，并不会进行类型检查。
