// 声明的类型并不确定，可以为多个类型中的一个

let a: number | string = 'a'
let b: number | string = 1

// 联合类型通常与 null 或 undefined 一起使用：

const sayHello = (name: string | undefined) => {
  /* ... */
}

// 有时候不仅要限制变量的类型，还需要限定变量的取值在某个特定的范围内

let num: 1 | 2 = 1
type EventNames = 'click' | 'scroll' | 'mousemove'

// 以上示例中的 1、2 或 'click' 被称为字面量类型，用来约束取值只能是某几个值中的一个。
