// 有的时候，我们希望给一个接口添加任意属性，可以通过索引类型来实现

interface Person {
  name: string
  age?: number
  //使用 [propName: string] 定义了任意属性取 string 类型的值
  [propName: string]: any
}
// 一旦定义了任意属性，那么确定属性和可选属性的类型都必须是它的类型的子集：
const p1 = {name: 'semlinker'}
const p2 = {name: 'lolo', age: 5}
const p3 = {name: 'kakuqo', sex: 1}

// 数字类型索引
interface Point {
  x: number
  y: number

  [prop: number]: number
}

// 字符串类型索引
interface Point1 {
  x: number
  y: number
  [prop: string]: number
}

// 数字索引是字符串索引的子类型

// 注意： 索引签名参数类型必须为 string 或 number 之一，但两者可同时出现

interface Point2 {
  [prop: string]: string
  [prop1: number]: string
}

// 注意：当同时存在数字类型索引和字符串类型索引的时候，数字类型的值类型必须是字符串类型的值类型或子类型

interface Point3 {
  [prop1: string]: string
  [prop2: number]: number // 错误
}

interface Point4 {
  [prop1: string]: Object
  [prop2: number]: Date // 正确
}
