function push<T>(array: T[], ...items: T[]): void {
  items.forEach(function (item) {
    array.push(item);
  });
}

let a: number[] = [];
push(a, 1, 2, 3);

console.log(a); // [1, 2, 3]