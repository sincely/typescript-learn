// interface 和 type 可以描述一个对象或者函数，
// type 可以声明基本类型别名，联合类型，元组等类型等等，interface不行
// interface 支持声明合并，可以被 extends 和 implements，type 不支持
// 1：接口可以被继承，类型不能被继承。

interface Person {
  name: string
  age: number
}

interface Employee extends Person {
  company: string
}

const employee: Employee = {name: 'John', age: 30, company: 'Acme Inc.'}

// 2：类型只能定义一次，而接口可以定义多次并且会自动合并。

type Status = 'active' | 'inactive'

const status1: Status = 'active'

// 如果试图再次定义同名类型，则会报错。

// 接口可以定义多次，并且会自动合并同名属性，例如：
interface Person {
  name: string
}

interface Person {
  age: number
}

const person: Person = {name: 'John', age: 30}

// 3：接口可以定义可选属性和只读属性，而类型不能定义可选属性和只读属性。
interface Person1 {
  name: string
  age?: number
  readonly email: string
}

const person1: Person1 = {name: 'John', email: 'john@example.com'}
const person2: Person1 = {name: 'Jane', age: 25, email: 'jane@example.com'}

person1.email = 'jane@example.com' // Error: Cannot assign to 'email' because it is a read-only property.

// 4：类型可以定义联合类型和交叉类型，而接口不能。
// 类型可以用联合类型和交叉类型来组合多个类型，例如：
type Person2 = {name: string} & {age: number}
type Status2 = 'active' | 'inactive'
type UserStatus = {status: Status2} & Person2

const userStatus: UserStatus = {name: 'John', age: 30, status: 'active'}

// 5：接口可以定义索引签名，而类型不能。
// 接口可以定义索引签名，TS的索引签名必须是 string 或者 number。symbols 也是有效的，TS 支持它。例如：
interface Person3 {
  [key: string]: string | number
}

const dict: Person3 = {a: 1, b: 2}

// 结论：
// 1：在对象扩展情况下，interface 使用 extends 关键字，而 type 使用交叉类型（&）。
// 2：同名的 interface 会自动合并，并且在合并时会要求兼容原接口的结构。
// 3：interface 与 type 都可以描述对象类型、函数类型、Class 类型，但 interface 无法像 type 那样表达元组、一组联合类型等等。
// 4：interface 无法使用映射类型等类型工具，也就意味着在类型编程场景中我们还是应该使用 type 。
// interface 就是描述对象对外暴露的接口，其不应该具有过于复杂的类型逻辑，最多局限于泛型约束与索引类型这个层面。
// 而 type alias 就是用于将一组类型的重命名，或是对类型进行复杂编程。
