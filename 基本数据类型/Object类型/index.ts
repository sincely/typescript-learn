// ：它是所有 Object 类的实例的类型，它可以作为所有对象的数据类型来使用。
// 包含所有类型的实例
let a: Object = 123
let a1: Object = '123'
let a2: Object = []
let a3: Object = {}
let a4: Object = () => 213

// 小写 object 类型
// 用于描述非原始类型，也就是除 number，string，boolean，symbol，null 或 undefined 之外的类型。
// 也就是说除了基本类型之外的类型(即引用类型)都可以被赋值给object类型。
// let b: object = 123 // Error
// let b1: object = '123' // Error
let b2: object = [] // Ok
let b3: object = {} // Ok
let b4: object = () => 213 // Ok

// {} 类型描述了一个没有成员的对象。当你试图访问这样一个对象的任意属性时，TypeScript 会产生一个编译时错误。
// 等价于 new Object() 或 Object.create(null)

// Type {}
const obj = {}

// Error: Property 'prop' does not exist on type '{}'.
// obj.prop = 'semlinker'

// 但是，你仍然可以使用在 Object 类型上定义的所有属性和方法，这些属性和方法可通过 JavaScript 的原型链隐式地使用：

// Type {}
const obj1 = {}

// "[object Object]"
obj1.toString()
