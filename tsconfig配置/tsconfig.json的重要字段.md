files - 设置要编译的文件的名称；
include - 设置需要进行编译的文件，支持路径模式匹配；
exclude - 设置无需进行编译的文件，支持路径模式匹配；
compilerOptions - 设置与编译流程相关的选项。
