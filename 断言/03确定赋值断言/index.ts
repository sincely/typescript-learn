// 在 TypeScript 2.7 版本中引入了确定赋值断言，即允许在实例属性和变量声明后面放置一个 ! 号，
// 从而告诉 TypeScript 该属性会被明确地赋值。为了更好地理解它的作用，我们来看个具体的例子：

let x: number
initialize()
// Variable 'x' is used before being assigned.(2454)
console.log(2 * x) // Error

function initialize() {
  x = 10
}

// 很明显该异常信息是说变量 x 在赋值前被使用了，要解决该问题，我们可以使用确定赋值断言：

// let x!: number;
// initialize();
// console.log(2 * x); // Ok

// function initialize() {
//   x = 10;
// }

// 通过 let x!: number; 确定赋值断言，TypeScript 编译器就会知道该属性会被明确地赋值。
