let myBooks = ['book1', 'book2', 'book3'];

myBooks.forEach(() => console.log('reading'));

myBooks.forEach(title => console.log(title));

myBooks.forEach((title, idx,) =>
  console.log(idx + '-' + title)
);

myBooks.forEach((title, idx,) => {
  console.log(idx + '-' + title);
});


// 未使用箭头函数
function book1() {
  let self = this;
  self.publishDate = 2016;
  setInterval(function () {
    console.log(self.publishDate);
  }, 1000);
}

// 使用箭头函数
function book2() {
  this.publishDate = 2016;
  setInterval(() => {
    console.log(this.publishDate);
  }, 1000);
}
