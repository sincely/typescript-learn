// 在 TypeScript 语法中，类型的标注主要通过类型后置语法来实现：“变量: 类型”
// https://juejin.cn/post/6997576373728444446
let num = 996
let num1: number = 996

// 上面代码中，第一行的语法是同时符合 JavaScript 和 TypeScript 语法的，
// 这里隐式的定义了num是数字类型，我们就不能再给num赋值为其他类型。
// 而第二行代码显式的声明了变量num是数字类型，同样，不能再给num赋值为其他类型，否则就会报错。
// 在 JavaScript 中，原始类型指的是非对象且没有方法的数据类型，
// 包括：number、boolean、string、null、undefined、symbol、bigInt。

// JavaScript原始基础类型     |	 TypeScript类型
//      number	            |    number
//      boolean	            |    boolean
//      string	            |    string
//      null	              |    null
//      undefined	          |    undefined
//      symbol	            |    symbol
//      bigInt	            |    bigInt

// 需要注意number和Number的区别：TypeScript中指定类型的时候要用 number ，
// 这是TypeScript的类型关键字。而 Number 是 JavaScript 的原生构造函数，
// 用它来创建数值类型的值，这两个是不一样的。
// 包括string、boolean等都是TypeScript的类型关键字，而不是JavaScript语法。
