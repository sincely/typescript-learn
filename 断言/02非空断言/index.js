// 在上下文中当类型检查器无法断定类型时，一个新的后缀表达式操作符! 可以用于断言操作对象是非 null 和非 undefined 类型。
// 具体而言，x! 将从 x 值域中排除 null 和 undefined 。
// 那么非空断言操作符到底有什么用呢？下面我们先来看一下非空断言操作符的一些使用场景。
// 1.忽略 undefined 和 null 类型
function myFunc(maybeString) {
    // Type 'string | null | undefined' is not assignable to type 'string'.
    // Type 'undefined' is not assignable to type 'string'.
    var onlyString = maybeString; // Error
    var ignoreUndefinedAndNull = maybeString; // Ok
}
function myFunc1(numGenerator) {
    // Object is possibly 'undefined'.(2532)
    // Cannot invoke an object which is possibly 'undefined'.(2722)
    var num1 = numGenerator(); // Error
    var num2 = numGenerator(); //OK
}
// 因为 ! 非空断言操作符会从编译生成的 JavaScript 代码中移除，所以在实际使用的过程中，要特别注意。比如下面这个例子：
var a = undefined;
var b = a;
console.log(b);
// 以上 TS 代码会编译生成以下 ES5 代码：
// "use strict";
// const a = undefined;
// const b = a;
// console.log(b);
// 虽然在 TS 代码中，我们使用了非空断言，使得 const b: number = a!;
// 语句可以通过 TypeScript 类型检查器的检查。
// 但在生成的 ES5 代码中，!非空断言操作符被移除了，所以在浏览器中执行以上代码，在控制台会输出 undefined。
