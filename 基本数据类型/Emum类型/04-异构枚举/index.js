// 异构枚举的成员值是数字和字符串的混合： 从技术的角度来说，枚举可以混合字符串和数字成员，但是似乎你并不会这么做：
// 除非你真的想要利用JavaScript运行时的行为，否则我们不建议这样做。
var Enum;
(function (Enum) {
    Enum[Enum["A"] = 0] = "A";
    Enum[Enum["B"] = 1] = "B";
    Enum["C"] = "C";
    Enum["D"] = "D";
    Enum[Enum["E"] = 8] = "E";
    Enum[Enum["F"] = 9] = "F";
})(Enum || (Enum = {}));
// 以上代码对于的 ES5 代码如下：
// "use strict";
// var Enum;
// (function (Enum) {
//     Enum[Enum["A"] = 0] = "A";
//     Enum[Enum["B"] = 1] = "B";
//     Enum["C"] = "C";
//     Enum["D"] = "D";
//     Enum[Enum["E"] = 8] = "E";
//     Enum[Enum["F"] = 9] = "F";
// })(Enum || (Enum = {}));
// 通过观察上述生成的 ES5 代码，我们可以发现数字枚举相对字符串枚举多了 “反向映射”：
console.log(Enum.A); //输出：0
console.log(Enum[0]); // 输出：A
