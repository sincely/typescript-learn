enum Direction {
  NORTH,
  SOUTH,
  EAST,
  WEST
}

let dir: Direction = Direction.NORTH
// 默认情况下，NORTH 的初始值为 0，其余的成员会从 1 开始自动增长。
// 换句话说，
// Direction.SOUTH 的值为 1
// Direction.EAST 的值为 2，
// Direction.WEST 的值为 3。
// 以上的枚举示例经编译后，对应的 ES5 代码如下：

// "use strict";
// var Direction;
// (function (Direction) {
//   Direction[(Direction["NORTH"] = 0)] = "NORTH";
//   Direction[(Direction["SOUTH"] = 1)] = "SOUTH";
//   Direction[(Direction["EAST"] = 2)] = "EAST";
//   Direction[(Direction["WEST"] = 3)] = "WEST";
// })(Direction || (Direction = {}));
// var dir = Direction.NORTH;

// 当然我们也可以设置 NORTH 的初始值，比如：
enum Direction2 {
  NORTH = 3,
  SOUTH,
  EAST,
  WEST
}
// 此时，Direction2.NORTH的值为3，其余的成员会从3开始自动增长。

let dir2: Direction2 = Direction2.NORTH
