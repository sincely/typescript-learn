// 可以借助 type 给类型起一个新名字。这里 type 只是创建一个新名字来引用那个类型，并不会新建一个类型。
type myString = string
let userName: myString = ''

type stringOrNumber = string | number
let myVariable: stringOrNumber = 1

interface people {
  name: string
}
type myPeople = people

let p1: myPeople = {
  name: 'mike',
}

// 也可以使用 type 来描述一个函数
type userInfo = (age: number, name: string) => string

let exampleFuncOne: userInfo = function (age: number, name: string) {
  return `${name}今年${age}岁了`
}
