// 在可选属性名字定义的后面加一个?符号，表示该属性“可有可无”

interface Person {
  readonly name: string
  age?: number
}

// 在只读属性名字定义的前面加上关键词 readonly，表示该属性赋值后不可再改变
interface IUser {
  readonly name: string
  readonly age: number
}
let myUser: IUser = {
  name: 'mike',
  age: 18,
}
myUser.age = 20 // error
