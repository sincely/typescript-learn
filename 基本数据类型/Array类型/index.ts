let list1: number[] = [1, 2, 3]
// ES5：var list1 = [1,2,3];

let list2: Array<number> = [1, 2, 3] // Array<number>泛型语法
// ES5：var list2 = [1,2,3];
