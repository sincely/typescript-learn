// 在 TypeScript 中交叉类型是将多个类型合并为一个类型。
// 通过 & 运算符可以将现有的多种类型叠加到一起成为一种类型，它包含了所需的所有类型的特性。
// 将多个类型合并为一个类型，新的类型将具有所有类型的特性，所以交叉类型特别适合对象过多的场景。交叉类型实际上是取所有类型的并集
type PartialPointX = {x: number}
type Point = PartialPointX & {y: number}

let point: Point = {
  x: 1,
  y: 1,
}

// 在上面代码中我们先定义了 PartialPointX 类型，接着使用 & 运算符创建一个新的 Point 类型，
// 表示一个含有 x 和 y 坐标的点，然后定义了一个 Point 类型的变量并初始化。

interface DogInterface {
  run(): void
}

interface CatInterface {
  jump(): void
}

// pet变量将具有两个接口类型的所有方法
/*
不能将类型“{}”分配给类型“DogInterface & CatInterface”。
  类型 "{}" 中缺少属性 "run"，但类型 "DogInterface" 中需要该属性。ts(2322)
*/
let pet: DogInterface & CatInterface = {
  run() {},
  jump() {},
}
