// keyof any 包含: string | number | symbol
type Record<K extends keyof any, T> = {
  // 表示键类型是约束于 K (string | number | symbol) 中的一种或多种，值类型为 T
  // in 表示遍历
  [P in K]: T
}

const foo: Record<string, boolean> = {
  a: true,
}
const bar: Record<'x' | 'y', number> = {
  x: 1,
  y: 2,
}
