type Partial<T> = {
  // 将原始类型 T 的所有属性加上 ? 修饰，变成可选的
  [P in keyof T]?: T[P]
}

interface Foo {
  a: string
  b: number
}

const foo: Partial<Foo> = {
  b: 2, // `a` 已经不是必须的了
}

// https://juejin.cn/post/6873080212675166215#heading-15
