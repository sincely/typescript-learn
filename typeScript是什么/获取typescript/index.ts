// 1.安装 TypeScript
// npm install -g typescript

// 2.验证 TypeScript 是否安装成功
// tsc -v
// Version 4.0.2

// 3.编译 TypeScript 文件
// tsc helloworld.ts
// helloworld.ts => helloworld.js
