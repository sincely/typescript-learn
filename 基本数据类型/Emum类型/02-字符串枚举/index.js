// 在一个字符串枚举里，每个成员都必须用字符串字面量，或另外一个字符串枚举成员进行初始化。
var Direction;
(function (Direction) {
    Direction["NORTH"] = "NORTH";
    Direction["SOUTH"] = "SOUTH";
    Direction["EAST"] = "EAST";
    Direction["WEST"] = "WEST";
})(Direction || (Direction = {}));
// 以上代码对应的 ES5 代码如下：
// "use strict";
// var Direction;
// (function (Direction) {
//     Direction["NORTH"] = "NORTH";
//     Direction["SOUTH"] = "SOUTH";
//     Direction["EAST"] = "EAST";
//     Direction["WEST"] = "WEST";
// })(Direction || (Direction = {}));
