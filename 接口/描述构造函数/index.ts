interface People {
  run: () => void
}

interface PeopleConstructor {
  new (name: string): People
}

class Student implements People {
  constructor(name: string) {}
  run() {
    console.log('run')
  }
}

function createPeople(construction: PeopleConstructor, name: string): People {
  return new construction(name)
}

let stu = createPeople(Student, '小明')
