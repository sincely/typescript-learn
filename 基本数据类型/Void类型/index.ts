// 某种程度上来说，void 类型像是与any类型相反，它表示没有任何类型。
// 表示没有任何类型,比如一些函数没有返回值，可以把这个函数设置为 void 型

// 声明函数返回值为void
function warnUser(): void {
  console.log('This is my warning message')
}

// 需要注意的是，声明一个void类型的变量没有什么作用，因为在严格模式下，它的值只能为 undefined
let unusable: void = undefined
console.log(unusable)
